using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnpuerta : MonoBehaviour
{
    public GameObject SpawnObject;
    public bool spawned;
    public Gamemanager game;

    // Start is called before the first frame update
    void Start()
    {
        this.spawned = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        float secs = game.seconds;
        if (secs > 50&&!spawned)
        {
            GameObject newspawn = Instantiate((GameObject)SpawnObject);
            this.spawned = true;
        }
    }
}
