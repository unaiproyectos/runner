using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour

{
    public Gamemanager game;

    // Start is called before the first frame update
    void Start()
    {
        game.seconds = 0;
    }

    // Update is called once per frame
    void Update()
    {
        float secs = game.seconds;

        game.seconds = secs + Time.deltaTime;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + (int)game.seconds;
        if (secs<30) 
        {
            game.vectobicho = new Vector2(game.vectobicho.x - secs * 0.0001f, 0);
        }
  
    }
}
