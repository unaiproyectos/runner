using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    public float Seconds=1;
    public Gamemanager game;
    public GameObject SpawnObject;
    public GameObject SpawnObject2;
    public int vida=20;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        if (this.vida <= 0)
        {

            SceneManager.LoadScene("WinScene");

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag=="Shoot") 
        {
            
            Destroy(collision.gameObject);
            game.vida--;
            this.vida--;
        }
    }
    private IEnumerator Spawn ()
    {
        while (true)
        {
            float rand = Random.Range(0, 3);
            if (rand==0|| rand == 1) 
            {
                yield return new WaitForSeconds(this.Seconds);
                
                    GameObject newspawn = Instantiate((GameObject)SpawnObject);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                
            }
            if (rand == 2)
            {
                yield return new WaitForSeconds(this.Seconds);
                
                    GameObject newspawn = Instantiate((GameObject)SpawnObject2);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+2f, this.transform.position.z);
                
            }

        }
        

    }
        
}
