using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAlert : MonoBehaviour
{
    public Gamemanager game;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (game.seconds>=50)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Sigue huyendo o\r\nEntra por la puerta\r\n";
        }
        
    }
}
