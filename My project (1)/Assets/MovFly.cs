using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovFly : MonoBehaviour
{
    public Vector2 vector = new Vector2(-5, 0);
    public Gamemanager game;
    // Start is called before the first frame update
    void Start()
    {
        
        this.GetComponent<Rigidbody2D>().velocity = game.vectobicho;
    }

    // Update is called once per frame
    void Update()
    {


        if (game.seconds > 14)
        {
            this.GetComponent<Rigidbody2D>().velocity = game.vectobicho;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
        }




    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "DedLine")
        {
            
            Destroy(this.gameObject);
        }
    }
    
}
