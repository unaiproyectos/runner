using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Shoot : MonoBehaviour
{
    public GameObject SpawnObject;
    
    
    private bool canshoot;
    // Start is called before the first frame update
    void Start()
    {

        canshoot = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.E)&&canshoot)
        {
            
            canshoot = false;
            
            GameObject newspawn = Instantiate((GameObject)SpawnObject);
            newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            StartCoroutine(Chocoboshoot());

        }
        

    }
    
    
    

    private IEnumerator Chocoboshoot() 
    {

        yield return new WaitForSeconds(1);
        canshoot = true;

    }
    
}
