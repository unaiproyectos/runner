using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arbolfondo : MonoBehaviour
{
    public Vector2 vector = new Vector2(-5, 0);
    public Vector2 vector2 = new Vector2(-6, 0);
    public Vector2 vector3 = new Vector2(-7, 0);
    public Vector2 vector4 = new Vector2(-8, 0);
    public Gamemanager game;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = vector;
    }

    // Update is called once per frame
    void Update()
    {
        float secs = game.seconds;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x - secs * 0.00001f, 0);
        if (this.transform.position.x < -10.2)
        {
            this.transform.position = new Vector2(16.4f, this.transform.position.y);
        }
    }
}
