using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChocobControler : MonoBehaviour
{
    public bool salto=true;
    private bool jumping;
    private bool stopjumping;

    // Start is called before the first frame update
    void Start()
    {
        jumping = false;
        stopjumping = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        //Movement
        if (Input.GetKey(KeyCode.D)) 
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(4, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if(Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0,this.GetComponent<Rigidbody2D>().velocity.y);
        }
        //Jump
        if (Input.GetKeyDown(KeyCode.Space)&& this.salto && !jumping) 
        {   
            this.salto = false;
            jumping = true;
            stopjumping = false;
            StartCoroutine(saltando());
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 130)); 
            //Debug.Log("Funciona");    
        }
        else if (Input.GetKey(KeyCode.Space)&&!stopjumping)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0.001f),ForceMode2D.Impulse);
        }
        else if(jumping)
        {
            stopjumping = true;
        }
        
    }
    private IEnumerator saltando()
    {
        yield return new WaitForSeconds(0.5f);
        stopjumping = true;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Jump recarge
        if (collision.transform.tag == "Land")
        {
            this.salto = true;
            this.jumping = false;
        }
        if (collision.transform.tag == "Tree"||collision.transform.tag=="Volador"|| collision.transform.tag == "Fire")
        {
           
            
            SceneManager.LoadScene("GameOver");
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Celestial_gate")
        {
            SceneManager.LoadScene("Boss");
        }
    }
}
